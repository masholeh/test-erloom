<?php

namespace App\Http\Controllers;

use App\UserTest;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index()
    {
    	$users = UserTest::all();

        return view('index', compact('users'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'    => 'required|string|min:1|max:100',
        ]);

        $count = UserTest::count();

        $count%2==0 ? $parity = 'Even' : $parity = 'Odd';

        $user = New UserTest;
        $user->name = $request->name;
        $user->parity = $parity;
        $user->save();

        return redirect()->route('index');
    }

}
